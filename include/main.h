#ifndef main_h
#define main_h

WINDOW* createWindow(int height, int width, int startY, int startX);
void destroyWindow(WINDOW* window);

int main(void);

SCREEN* initNcurses();
void getInitialInput();
void mainLoop(char** strArray, WINDOW* windows[], int strArrSize, int numWindows);
void endApp(char** strArr, SCREEN* scr);

#endif
