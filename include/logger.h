#ifndef logger_h
#define logger_h

#include <stdio.h>
#include <errno.h>
#include <string.h>

int isFileNull(FILE* file);
void logString(FILE* file, const char* text);
void readString(FILE* file, char *string);
void logChar(FILE* file, int character);

#endif
