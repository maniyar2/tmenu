#ifndef windows_h
#define windows_h

#include <ncurses.h> 

WINDOW* createWindow(int height, int width, int startY, int start);
void destroyWindow(WINDOW* window);
void splitInto(WINDOW* windows[], int size, int startX, int width, int height);

#endif 
