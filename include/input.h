#ifndef input_h
#define input_h

char* getInput();
char** splitString(char* string, char* delimiter);

#endif
