#include "logger.h"

int isFileNull(FILE* file) {
    if (file == NULL) {
        fprintf(stderr, "Error opening log file: %s\n", strerror(errno));
        return 1;
    } else {
        return 0;
    }
}

void logString(FILE* file, const char *text) 
{
    fprintf(file, "%s", text); 
    fflush(file);
}

void logChar(FILE* file, int character) 
{
    fputc(character, file);
    fflush(file);
}

void readString(FILE* file, char *string) 
{
    fscanf(file, "%s", string);
    fflush(file);
}


