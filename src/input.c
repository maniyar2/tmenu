#include "input.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* getInput()
{
    unsigned int maxLength = 100;
    unsigned int currentSize = 0;

    char *pStr = malloc(maxLength);
    currentSize = maxLength;

    if (pStr != NULL) 
    {
        int c = EOF;
        unsigned int i = 0;
        while ((c = getchar()) != EOF)
        {
            pStr[i++] = (char) c;

            if (i == currentSize)
            {
                currentSize = i + maxLength;
                pStr = realloc(pStr, currentSize);
            }
        }
        pStr[i] = '\0';
    }
    return pStr;
}

char** splitString(char* string, char* delimiter) 
{
    unsigned int currentSize = 5;
    unsigned int i = 0;

    char **stringPointers = malloc(currentSize * sizeof(char*));
    char *ch = strtok(string, "\n");
    while(ch != NULL)
    {
        stringPointers[i++] = (char*) ch;
        if (i == currentSize) 
        {
            currentSize += i;
            stringPointers = realloc(stringPointers, currentSize * sizeof(char*));
        }
        ch = strtok(NULL, "\n");
    }
    stringPointers[i] = NULL;
    return stringPointers;
}





