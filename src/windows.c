#include "windows.h"

#include <ncurses.h> 

WINDOW* createWindow(int height, int width, int startY, int startX) 
{
    WINDOW* window = newwin(height, width, startY, startX); //whomst the fuck (y, x) everything
    box(window, 0, 0);
    wrefresh(window);
    refresh();
    return window;
}

void destroy_win(WINDOW* local_win)
{	
	wborder(local_win, ' ', ' ', ' ',' ',' ',' ',' ',' ');
	/* The parameters taken are 
	 * 1. win: the window on which to operate
	 * 2. ls: character to be used for the left side of the window 
	 * 3. rs: character to be used for the right side of the window 
	 * 4. ts: character to be used for the top side of the window 
	 * 5. bs: character to be used for the bottom side of the window 
	 * 6. tl: character to be used for the top left corner of the window 
	 * 7. tr: character to be used for the top right corner of the window 
	 * 8. bl: character to be used for the bottom left corner of the window 
	 * 9. br: character to be used for the bottom right corner of the window
	 */
	wrefresh(local_win);
	delwin(local_win);
}

void splitInto(WINDOW* windows[], int size, int startX, int width, int height)
{
  for (int i = 0; i < size; i++)
  {
    windows[i] = createWindow(height/ size, width, height * i/size, startX);
  }
}
