#include "logger.h"
#include "windows.h"
#include "main.h"
#include "input.h"
#include "util.h"

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <ncurses.h>

enum { redBlack = 1 };

int main(void) 
{   
    char* str = getInput();
    char** strArray = splitString(str, "\n");

    int strArrSize = 0;
    char* string = strArray[strArrSize];
    while (string != NULL) 
    {
        string = strArray[++strArrSize];
    }

    SCREEN* scr = initNcurses();
    int height = LINES * 1;
    int width = COLS * 0.85;
    int startY = (LINES - height) / 2;	/* Calculating for a center placement */
    int startX = (COLS - width) / 2;	/* of the window		*/

    int size = min((height / 5), strArrSize);

    WINDOW* winArr[size];
    splitInto(winArr, size, startX, width, height);

    
    mainLoop(strArray, winArr, strArrSize, size);
    endApp(strArray, scr);

    return 0;
}

SCREEN* initNcurses() 
{
    FILE* term = fopen("/dev/tty", "r+");
    if (term == NULL) {
        perror("fopen(/dev/tty)");
        exit(EXIT_FAILURE);
    }   

    SCREEN* scr = newterm(NULL, term, term);  // Initialize screen
    set_term(scr);
    if (has_colors() == TRUE)
    {
        init_pair(redBlack, COLOR_RED, COLOR_BLACK);
    } else
    {
        endwin();
        delscreen(scr);
        fprintf(stderr, "Your terminal does not support color.");
        exit(EXIT_FAILURE);
    }

    cbreak();   
    noecho();   // Don't echo back characters 
    keypad(stdscr, TRUE);  // Allow wacky characters
    curs_set(0);
}

void mainLoop(char** strArray, WINDOW* windows[], int strArrSize, int numWindows) 
{
    int strArrCounter = 0;

    char ch = 0;
    while (ch != 'q') 
    {
        
        if ((ch == 'j' || ch == 2) && (strArrCounter + (numWindows/2 + 1)) < strArrSize) // It works, don't change it.
        {
            strArrCounter++;
        }
        else if ((ch == 'k' || ch == 3) && strArrCounter > (-numWindows/2)) // 3 is up, 2 is down 
        {
            strArrCounter--;
        }
        else if (ch == 10) // 10 = enter/return key
        {
            fprintf(stdout, strArray[(strArrCounter+(numWindows/2))]);
            break;
        }

        for (int ind = 0; ind < numWindows; ind++)
        {
            if ((strArrCounter + ind) > -1 && (strArrCounter + ind) < strArrSize)
            {
                if (ind == numWindows/2)
                {
                        attron(COLOR_PAIR(redBlack));
                        mvwprintw(windows[ind], 1, 4, "* %s", strArray[strArrCounter + ind]);
                        attroff(COLOR_PAIR(redBlack));
                } 
                else
                {
                    mvwprintw(windows[ind], 1, 4, strArray[strArrCounter + ind]);
                }
                box(windows[ind], 0, 0);
                wrefresh(windows[ind]);
                wclear(windows[ind]);
            } else
            {
                wrefresh(windows[ind]);
            }
        }

        ch = wgetch(stdscr);
    }
}

void endApp(char** strArr, SCREEN* scr)
{
    endwin();
    delscreen(scr); 
    free(strArr[0]);
    free(strArr);
}



