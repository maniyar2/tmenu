#ifndef test_h
#define test_h

#include "main.h"
#include "logger.h"
#include "input.h"

#include <assert.h>
#include <string.h>

void testReadWrite();
void testInput();
void testSplit(char* str);

#endif
